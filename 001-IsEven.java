// https://tests.mettl.com/authenticateKey/2bd025dc

import java.util.*;

class IsEven {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        IsEven check = new IsEven();
        int input1 = sc.nextInt();
        System.out.println(check.isEven(input1));
        sc.close();
    }

    public int isEven(int input1) {
        if (input1 % 2 == 0) {
            return 2;
        } else {
            return 1;
        }
    }
}